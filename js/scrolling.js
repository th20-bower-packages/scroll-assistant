;(function ($, window, document, undefined) {

    $window = $(window);

    var scrollAssistant = {
        inTransition: false,

        attachEvents: function () {
            var _this = this;

            $(document).on('mousewheel DOMMouseScroll', function(event) {
                _this.onScroll(event);
            });
        },

        onScroll: function (event) {
            var _this = this,
                delta = event.originalEvent.wheelDelta || -event.originalEvent.detail,
                viewportTop = $window.scrollTop(),
                windowHeight = $window.height(),
                target;

            event.preventDefault();

            if (this.inTransition) {
                $('html, body').stop();
            }

            this.inTransition = true;

            var scroll = delta < 0 ? 250 : -250;

            $('html, body').animate(
                {'scroll-top': viewportTop + scroll + 'px'},
                500,
                'linear',
                function () { _this.inTransition = false }
            );
        }
    }

    window.scrollAssistant = scrollAssistant;

    $(function () {
        scrollAssistant.attachEvents();
    });

})(jQuery, this, this.document);
